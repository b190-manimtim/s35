const express = require("express");

//this lets us use the mongoose module 
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// SECTION - MongoDB Connection
// Connect to the data by passing in your connection string (from MongoDB)
// b190-to-do is the database the we have created in our MongoDB

mongoose.connect("mongodb+srv://chanomanimtim:12455421@wdc028-course-booking.ivug0tv.mongodb.net/b190-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

// user Schema ---------- ACTIVITY
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})



// SECTION - Models
// models in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// Models use Schemas and the act as the middleman from the server to the database

const Task = mongoose.model("Task", taskSchema);

// User model ---------- ACTIVITY
const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Create a new task
// BUSINESS LOGIC - add a functionality that will check if there are duplicate tasks

app.post("/tasks", (req,res) => {
    Task.findOne({name: req.body.name}, (err, result) =>{
        if (result !== null && result.name === req.body.name){
            return res.send("Duplicate task found")
        } else {
            let newTask = new Task({
                name: req.body.name
            })
            newTask.save((saveErr, savedTask) => {
                if (saveErr){
                    return console.error(saveErr)
                } else{
                    return res.status(201).send("New task created")
                }
            })
        }
    } )
})

// POST sign up ---------- ACTIVITY
app.post("/signup", (req,res) => {
    Task.findOne({username: req.body.username}, (err, result) =>{
        if (result !== null && result.name === req.body.name){
            return res.send("Duplicate user found")
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save((saveErr, savedTask) => {
                if (saveErr){
                    return console.error(saveErr)
                } else{
                    return res.status(201).send("New user created")
                }
            })
        }
    } )
})

// Mini activity
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) =>{
        if (err ){
            return console.log(err)
        } else {
        return res.status(200).json(result)
        
        }
    } )
});

// app.get("/tasks",(req,res)=>{
// 	Task.find({}, (err,result) => {
// 		if(result === null){
// 			return res.send("Error")
// 		}else{
// 			res.send(result)
// 		}
// 	})
// })


app.listen(port, () => console.log(`Server running at port: ${port}`));